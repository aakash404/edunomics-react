var express = require('express');
var router = express.Router();
var func = require('../func.js');
var edo=require('../edonomix.js');
var con = require('../db');

// router.get('/',func.auth,function(req, res, next) 
// {
// 	res.json({"success":true,'msg':'add cs page'});     
// });

router.post('/',func.auth,function(req, res, next) 
{
  console.log(req.body);
  req.check('name','invalid cs name').isLength({min:1,max:100});

  var verrs=req.validationErrors();
  if(verrs)
  {
    res.json({ success:false,msg: verrs[0].msg,});
  }
  else
  {
     var cs = {
        cs_position:req.body.position,
        cs_name:req.body.name,
        cs_desc:req.body.desc,
        cs_image1:req.body.image1,
        cs_image2:req.body.image2,
        cs_image3:req.body.image3,
        cs_image4:req.body.image4,
    };
    con.query("Insert into cs SET ? ;",cs,function(err,result)
    {
      if(err)
      {
        console.log(err);
        res.json({success:false,msg: 'something went wrong',});
      }
      else
      {
        res.json({ success:true,msg: 'succesful entry', });
      }
    });
  }		   
});


module.exports = router;