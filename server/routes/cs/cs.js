var express = require('express');
var router = express.Router();

var add=require('./addcs');
router.use('/add', add);

var view=require('./viewcs');
router.use('/view', view);

var update=require('./updatecs');
router.use('/update', update);

var del=require('./deletecs');
router.use('/delete', del);

module.exports=router;