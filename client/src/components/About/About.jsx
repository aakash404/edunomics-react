import React, { Component } from 'react'
import './About.css'
import {Link} from 'react-router-dom'
export class About extends Component {
  render() {
    return (
        <React.Fragment>
      <div>
      <div className = "blog-content text-center">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h1>About Us</h1>
                            </div>    
                        </div>

                    </div>
               </div>

        {/*conetent section */}
        <section className = "about-content" id="about">
   <div className = "container">
  <div className = "row">
        <div className = "col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <h1 className="text-center center-block">About Us </h1>
            <hr className="custom-hr" />
            <p>Helping clients to create the future </p>
            <p><em>Edunomics combines tech expertise and business intelligence to catalyse change and deliver results </em></p>
        </div>
  </div> 

 <div className="custom-about-para">
  <div className="row">
  <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <h4>What we do </h4>
                <p>As a incumbent in IT Services, Digital & Business Solutions, we partner with clients to simplify, strengthen and transform their business. </p>
                </div>

                 <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <h4>Our Customers </h4>
                <p>Today our customer expeience defines our brand, analytics overrule instincts and innovation, insights and agility not size defines winnes  </p>
                </div>

                 <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <h4>Our Talent</h4>
                <p>Edunomics is employing top talent to provide uniquely matching solution for your unique problem</p>
                </div>

                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <button type="button" className="btn btn-info custom-btn">READ MORE</button>
                </div>


                </div>
  </div>
    
    
   </div>

</section>
        {/*end content section */}
      </div>
      </React.Fragment>
    )
  }
}

export default About
