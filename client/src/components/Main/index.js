import React, { Component } from 'react';
import './main.css';
import {Link} from 'react-router-dom';

export class Header extends Component {
    constructor(props){
    super(props)

  this.state = {
        first_name : "",
        last_name : "",
        email : "",
        mobile : "",
        msg : "",
        quote_mail : ""
       
    
      };
      this.quotesubmit = this.quotesubmit.bind(this);
    }
      change  = e => {
        this.setState ({
          [e.target.name]: e.target.value
        });
      };
      getWebsite = () => {
            fetch("/")
        };
      onSubmit = e =>
      {
          e.preventDefault();
          console.log(this.state)
          this.state.name = this.state.first_name+" "+this.state.last_name;
          console.log(this.state.name);
          if (
                this.state.name === "" ||
                this.state.email === "" ||
                this.state.mobile === ""
             ) {
                alert("Unable to contact because fields were left blank");
                }else {
                    fetch(`contact_us/add`,{
                        method : "POST",
                        headers : {
                            "Content-Type": "application/json; charset=utf-8"
                        },
                        body: JSON.stringify(this.state) 
                    }
                    ).then(function(response){ 
                        return response.json();})
                    .then(function(json){
                         if(json.success===true){
                        //   console.log(json);
                        alert("your data has been submitted");
                        this.setState ({
                            first_name : "",
                            last_name : "",
                            email : "",
                            mobile : "",
                            msg : "",
                            quote_mail: ""
              
                        })
                    }
                    else{
                      alert(json.msg);
                      console.log(json);
                  }
                }.bind(this))
            }
      };

      quotesubmit = (e) =>
      {
          e.preventDefault();
          console.log(this.state)
          if (
                this.state.quote_mail === ""
             ) {
                alert("Unable to contact because fields were left blank");
                }else {
                    fetch(`enquiry/add`,{
                        method : "POST",
                        headers : {
                            "Content-Type": "application/json; charset=utf-8"
                        },
                        body: JSON.stringify(this.state) 
                    }
                    ).then(function(response){ 
                        return response.json();})
                    .then(function(json){
                         if(json.success===true){
                        //   console.log(json);
                        alert("your data has been submitted");
                        this.setState ({
                           quote_mail : ""
              
                        })
                    }
                    else{
                      alert(json.msg);
                      console.log(json);
                  }
                }.bind(this))
            }
      };
  render() {
    return (
      <div className="manual-home-content">
        {/*banner */}
        
        <section id="home" className="carousel slide" data-ride="carousel">

<ul className="carousel-indicators">
  <li data-target="#i1" data-slide-to="0" className="active"></li>
  <li data-target="#i2" data-slide-to="1"></li>
  <li data-target="#i3" data-slide-to="2"></li>
</ul>


<div className="carousel-inner custom-slider-height">
  <div className="carousel-item active" id="i1"  >
    <img src={require('../images/banner1.jpg')} className="img-fluid" />
  </div>
  <div className="carousel-item" id="i2">
    <img src={require('../images/banner2.jpg')} className="img-fluid"/>
  </div>
  <div className="carousel-item" id="i3">
    <img src={require('../images/banner3.jpg')} className="img-fluid"/>
  </div>
</div>


<a className="carousel-control-prev" href="#demo" data-slide="prev">
  <span className="carousel-control-prev-icon"></span>
</a>
<a className="carousel-control-next" href="#demo" data-slide="next">
  <span className="carousel-control-next-icon"></span>
</a>
</section>

        {/* end baner */}

        {/* ABOUT US CONTENT HERE */}
<section className = "about-content" id="about">
   <div className = "container">
  <div className = "row">
        <div className = "col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <h1 className="text-center center-block">About Us </h1>
            <hr className="custom-hr" />
            <p>Helping clients to create the future </p>
            <p><em>Edunomics combines tech expertise and business intelligence to catalyse change and deliver results </em></p>
        </div>
  </div> 

 <div className="custom-about-para">
  <div className="row">
  <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <h4>What we do </h4>
                <p>As a incumbent in IT Services, Digital & Business Solutions, we partner with clients to simplify, strengthen and transform their business. </p>
                </div>

                 <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <h4>Our Customers </h4>
                <p>Today our customer expeience defines our brand, analytics overrule instincts and innovation, insights and agility not size defines winnes  </p>
                </div>

                 <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <h4>Our Talent</h4>
                <p>Edunomics is employing top talent to provide uniquely matching solution for your unique problem</p>
                </div>

                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <button type="button" className="btn btn-info custom-btn">READ MORE</button>
                </div>


                </div>
  </div>
    
    
   </div>

</section>



        {/* END ABOUT US CONTENT HERE*/}

        {/* SERVICES CONTENT HERE */}
            <section className="home-services-content" id="services">
                <div className="container">
                    <div className="row">
                    <div className = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 className="text-center center-block">Services </h1>
        </div>
        </div>

        <div className = "row custom-first-row">
            <div className = "col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div className="creative-box text-center">
               <i className="fa fa-music fa-2x" aria-hidden="true"></i>
                    <h3>Website - APP</h3>
                    <p>Thorough business research for Presnting a best UI/UX designed Web-APP Enabling you grow well in your first step to digital strategy.  </p>
                </div>
            </div>

                 <div className = "col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div className="creative-box text-center">
                <i className="fa fa-magic fa-2x" aria-hidden="true"></i>
                    <h3>Tech Solution</h3>
                    <p>Every Industry needs a solution which fits it best and where we enable you to digitise your process perfectly to either save time or cost </p>
                </div>
            </div>

             <div className = "col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div className="creative-box text-center">
                <i className="fa fa-book fa-2x" aria-hidden="true"></i>
                    <h3>IOT &amp; AI </h3>
                    <p>You Operate a lots of resources, and they have data with them lets capture them with best IOT solutioning and presenting it with a capable dashboard. </p>
                </div>
            </div>
</div>
<div className="row">
             <div className = "col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div className="creative-box text-center">
                <i className="fa fa-file-text-o fa-2x" aria-hidden="true"></i>
                    <h3>Data Analytics </h3>
                    <p>Big Data is no more a Jargon its a need of hour to either enhance or optimise the process and presenting it beautifully.</p>
                </div>
            </div>

             <div className = "col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div className="creative-box text-center">
                <i className="fa fa-line-chart fa-2x" aria-hidden="true"></i>
                    <h3>Tech Strategy Consultation</h3>
                    <p>Tech is our forte, we can present you a digital roadmap to either develop suitable tech solutions or either adopt one </p>
                </div>
            </div>

             <div className = "col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div className="creative-box text-center">
                <i className="fa fa-tasks fa-2x" aria-hidden="true"></i>
                    <h3>Process Automation</h3>
                    <p>Gain Agility, accuracy and speed with process automation & best workflow management to enahance control too</p>
                </div>
            </div>
</div>
                    
                   
                
                </div>

            </section>



        {/* END SERVICES CONTENT HERE*/}

        {/* CLIEMT SLIDER HERE*/}
    <section className = "client-content" id="client">
        <div className="container">
            <div className="row">
            <div className = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 className="text-center center-block">Clients </h1>
        </div>
            </div>

<div class="col-md-12">
                    <div id="blogCarousel" class="carousel slide" data-ride="carousel">
                       
                        <div class="carousel-inner">

                            <div class="carousel-item active">
                                <div class="row">                               
                              <div class="col-md-4 eckcon">
                                       
                                         <img class="mediasliderimage img-fluid" src={require('../images/logo.png')} alt="Image" />
                                            <div class="overlay">
                                            <a href ={"http://www.rdcconcrete.com/"} target={"_blank"}><div class="text">RDC Concrete</div></a>
                                          </div>
                                       
                                    </div>
                                    <div class="col-md-4 eckcon">
                                       
                                            <img class="mediasliderimage img-fluid" src={require('../images/2020logo.png')} alt="Image" />
                                            <div class="overlay">
                                            <a href ={"http://www.2020-networks.net/"} target={"_blank"}><div class="text">2020 - Network</div></a>
                                          </div>
                                       
                                    </div>
                                    <div class="col-md-4 eckcon">
                                      
                                            <img class="mediasliderimage img-fluid" src={require('../images/site-logo.png')} alt="Image"  />
                                            <div class="overlay">
                                            <a href={"http://www.equipshare.in/"} target={"_blank"}><div class="text">Equipshare</div></a>
                                          </div>
                                       
                                    </div>
                                  
                                </div>
                               
                            </div>
                           
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-md-6 eckcon">
                                       
                                            {/* <img class="mediasliderimage" src={require("../images/020.jpg")} alt="Image"  /> */}
                                            <p>Distribution Management System </p>
                                            <div class="overlay">
                                            <div class="text">Clients</div>
                                          </div>
                                       
                                    </div>
                                    <div class="col-md-6 eckcon">
                                    
                                            <img class="mediasliderimage img-fluid" src={require('../images/nikhilcomforts.png')} alt="Image" />
                                            <div class="overlay">
                                            <a href={"http://www.nikhilcomforts.com/"} target={"_blank"}><div class="text">Nikhil Comforts</div></a>
                                          </div>
                                       
                                    </div>
                                    {/* <div class="col-md-3 eckcon">
                                      
                                            <img class="mediasliderimage" src={require("../images/022.jpg")} alt="Image"/>
                                            <div class="overlay">
                                            <div class="text">Clients</div>
                                          </div>
                                       
                                    </div>
                                    <div class="col-md-3 eckcon">
                                       
                                            <img class="mediasliderimage" src={require("../images/024.jpg")} alt="Image"/>
                                            <div class="overlay">
                                            <div class="text">Clients</div>
                                          </div>
                                       
                                    </div> */}
                                </div>
                               
                            </div>
                         

                        </div>
                       
                    </div>
                   

                </div>
 

        </div>
    </section>


        {/*END CLIENT HERE*/}

        {/*CONTACT HERE */}
<section className = "contact-content" id="contact">
    <div className = "container">
        <div className="row">
        <div className = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 className="text-center center-block">Contact Us </h1>
        </div>
        </div>

        <div className="row">
            <div className = "col-lg-6 col-md-6 col-sm-12 col-xs-12 custom-right-padding">
                <div className="custom-left-content">
                <form>
  <div className="form-row">
    <div className="form-group col-md-6">
      <label for="inputEmail4">First Name</label>
      <input type="text" className="form-control custom-contact-form-component" id="inputEmail4" name="first_name" value={this.state.first_name} onChange={e => this.change(e)} required/>
    </div>
    <div className="form-group col-md-6">
      <label for="inputPassword4">Last Name</label>
      <input type="text" className="form-control custom-contact-form-component" id="inputPassword4" name="last_name" value={this.state.last_name} onChange = {e=>this.change(e)} required/>
    </div>
  </div>

    <div className="form-row">
    <div className="form-group col-md-6">
      <label for="inputEmail4">Email</label>
      <input type="email" className="form-control custom-contact-form-component" id="inputEmail4" name="email" value={this.state.email} onChange={ e => this.change(e)} required/>
    </div>
    <div className="form-group col-md-6">
      <label for="inputPassword4">Phone Number</label>
      <input type="number" className="form-control custom-contact-form-component" id="inputPassword4" name="mobile" value={this.state.mobile} onChange = {e => this.change(e)} required/>
    </div>
  </div>
  <div className="form-group">
    <label for="inputAddress">Message</label>
    {/* <input type="text" className="form-control custom-contact-form-component" id="inputAddress" placeholder="1234 Main St" /> */}
    <textarea className = "form-control custom-contact-form-component" name="msg" value = {this.state.msg} onChange = {e => this.change(e)} required></textarea>
  </div>

 
  <button type="submit" className="btn btn-primary" onClick = {e => this.onSubmit(e)}>Contact Us</button>
</form>
                </div>
            </div>
                <div className = "col-lg-6 col-md-6 col-sm-12 col-xs-12 custom-right-padding">
                    <div className="custom-right-content">
                        <div className="row">
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h2>Contact Info</h2>
                            </div>
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <p> <i className="fa fa-map-marker" aria-hidden="true"></i>
                           &nbsp;&nbsp;&nbsp;  Edunomics Third Floor,Nikhil Heights,<br/>
                            Sajjan Wadi, Mulund East Mumbai
                           <br/>
                           Pincode 40081 
                            
                            </p>
                            </div>
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <p> <i className="fa fa-phone" aria-hidden="true"></i>
                           &nbsp;&nbsp;&nbsp; +91 9767460825</p>
                            </div>
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <p><i className="fa fa-envelope-o" aria-hidden="true"></i>
                           &nbsp;&nbsp;&nbsp; hr@edunomics.in</p>
                            </div>
                        </div>
                    </div>
                </div>

        </div>
    </div>
</section>



        {/* END CONTACT HERE */}

        {/* MAP CONTENT HERE */}
     
         
                    {/* <div className="map custom-map" id="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d117759.88917816881!2d75.847574!3d22.7283701!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1536126291854" width="600" height="450" frameborder="0" allowfullscreen></iframe>
	</div>
                  
              
       */}
       


        {/* END MAP CONTENT HERE */}
        {/*upper footer */}
        <section className="custom-upper-footer-section">
        <div className="container">
          <div className="row">
          <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
             <img src = {require('../images/footer-final.png')} className="img-fluid" />
            </div>

            <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <h4>Quick Links</h4>
                <ul>
                     <li>Startups</li>
                     <li>Enterprises</li>
                     <li>Consultation</li>   
                    </ul>
            </div>

            <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <h4>Policies</h4>
                <ul>
                     <li>Terms &amp; Conditions</li>
                     <li>Privacy Policy</li>
                     <li>NDA</li>   
                    </ul>
            </div>

            <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <h4>Ask For Quote</h4>
            <div class="input-group mb-3">
            <form>
            <div class="input-group mb-3">
  <input type="email" class="form-control" placeholder="john@example.com" aria-label="quote_mail" aria-describedby="basic-addon2" name = "quote_mail" value={this.state.quote_mail} onChange={e => this.change(e)}/>
  <div class="input-group-append">
    <button class="btn btn-outline-secondary" type="submit" onClick={this.quotesubmit.bind(this)}><i className="fa fa-paper-plane-o" aria-hidden="true"></i></button>
  </div>
</div>
  </form>
</div>
            </div>

          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className="custom-upper-footer-social text-center center-block">
             <ul className="list-inline">
               <a href= {'https://www.facebook.com/'} target="_blank"> <li className="list-inline-item">
                <i className="fa fa-facebook" aria-hidden="true"></i>
                </li></a>
               <a href ={"https://twitter.com/"} target="_blank"> <li className="list-inline-item">
                <i className="fa fa-twitter" aria-hidden="true"></i>
                </li></a>
               <a href= {"https://www.linkedin.com/"} target="_blank"><li className="list-inline-item">
                <i className="fa fa-linkedin" aria-hidden="true"></i>
                </li></a>
               <a href={"https://www.youtube.com/"} target="_blank"> <li className="list-inline-item">
                <i className="fa fa-youtube" aria-hidden="true"></i>
                </li></a>
               <a href={"https://plus.google.com/"} target="_blank"> <li className="list-inline-item">
                <i className="fa fa-google-plus" aria-hidden="true"></i>
                </li></a>
               <a href={"https://www.instagram.com/"} target="_blank"> <li className="list-inline-item">
                <i className="fa fa-instagram" aria-hidden="true"></i>
                </li></a>

            </ul>
            </div>
    </div>

          </div>
        </div>
      </section>
        {/*end upper footer */}
      </div>

    )
  }
}

export default Header;
