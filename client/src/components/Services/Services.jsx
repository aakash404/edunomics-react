import React, { Component } from 'react'
import './Services.css'
import {Link} from 'react-router-dom'
export class Services extends Component {
  render() {
    return (
        <React.Fragment>
      <div>
      <div className = "blog-content text-center">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h1>Services</h1>
                            </div>    
                        </div>

                    </div>
               </div>
        {/*services container */}
        <section className="services-content" id="services">
                <div className="container">
                    <div className="row">
                    {/* <div className = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 className="text-center center-block">Services </h1>
        </div> */}
        </div>

        <div className = "row custom-first-row">
            <div className = "col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div className="creative-box text-center">
               <i className="fa fa-music fa-2x" aria-hidden="true"></i>
                    <h3>Website - APP</h3>
                    <p>Thorough business research for Presnting a best UI/UX designed Web-APP Enabling you grow well in your first step to digital strategy.  </p>
                </div>
            </div>

                 <div className = "col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div className="creative-box text-center">
                <i className="fa fa-magic fa-2x" aria-hidden="true"></i>
                    <h3>Tech Solution</h3>
                    <p>Every Industry needs a solution which fits it best and where we enable you to digitise your process perfectly to either save time or cost </p>
                </div>
            </div>

             <div className = "col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div className="creative-box text-center">
                <i className="fa fa-book fa-2x" aria-hidden="true"></i>
                    <h3>IOT &amp; AI </h3>
                    <p>You Operate a lots of resources, and they have data with them lets capture them with best IOT solutioning and presenting it with a capable dashboard. </p>
                </div>
            </div>
</div>
<div className="row">
             <div className = "col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div className="creative-box text-center">
                <i className="fa fa-file-text-o fa-2x" aria-hidden="true"></i>
                    <h3>Data Analytics </h3>
                    <p>Big Data is no more a Jargon its a need of hour to either enhance or optimise the process and presenting it beautifully.</p>
                </div>
            </div>

             <div className = "col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div className="creative-box text-center">
                <i className="fa fa-line-chart fa-2x" aria-hidden="true"></i>
                    <h3>Tech Strategy Consultation</h3>
                    <p>Tech is our forte, we can present you a digital roadmap to either develop suitable tech solutions or either adopt one </p>
                </div>
            </div>

             <div className = "col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div className="creative-box text-center">
                <i className="fa fa-tasks fa-2x" aria-hidden="true"></i>
                    <h3>Process Automation</h3>
                    <p>Gain Agility, accuracy and speed with process automation & best workflow management to enahance control too</p>
                </div>
            </div>
</div>
                    
                   
                
                </div>

            </section>

        {/*end services container */}
      </div>        
      </React.Fragment>
    )
  }
}

export default Services
