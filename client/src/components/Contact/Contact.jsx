import React, { Component } from 'react'
import './Contact.css'
import {Link} from 'react-router-dom'
export class Contact extends Component {
	constructor(props){
		super(props)
	
	  this.state = {
			first_name : "",
			last_name : "",
			email : "",
			mobile : "",
			msg : "",
			quote_mail : ""
		   
		
		  };
		  
		}
		  change  = e => {
			this.setState ({
			  [e.target.name]: e.target.value
			});
		  };
		  getWebsite = () => {
				fetch("/")
			};
		  onSubmit = e =>
		  {
			  e.preventDefault();
			  console.log(this.state)
			  this.state.name = this.state.first_name+" "+this.state.last_name;
			  console.log(this.state.name);
			  if (
					this.state.name === "" ||
					this.state.email === "" ||
					this.state.mobile === ""
				 ) {
					alert("Unable to contact because fields were left blank");
					}else {
						fetch(`contact_us/add`,{
							method : "POST",
							headers : {
								"Content-Type": "application/json; charset=utf-8"
							},
							body: JSON.stringify(this.state) 
						}
						).then(function(response){ 
							return response.json();})
						.then(function(json){
							 if(json.success===true){
							//   console.log(json);
							alert("your data has been submitted");
							this.setState ({
								first_name : "",
								last_name : "",
								email : "",
								mobile : "",
								msg : "",
								quote_mail: ""
				  
							})
						}
						else{
						  alert(json.msg);
						  console.log(json);
					  }
					}.bind(this))
				}
		  };
  render() {
    return (
        <React.Fragment>
      <div>
	  <div className = "blog-content text-center">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h1>Contact</h1>
                            </div>    
                        </div>

                    </div>
               </div>

        {/*content here */}
        <div className="container-contact100 contact-custom-content">
		<div className="contact100-map" id="google_map" data-map-x="40.722047" data-map-y="-73.986422" data-pin="images/icons/map-marker.png" data-scrollwhell="0" data-draggable="1"></div>

		<div className="wrap-contact100">
			<div className="contact100-form-title custom-contact-form">
				<span className="contact100-form-title-1">
					Contact Us
				</span>

				<span className="contact100-form-title-2">
					Feel free to drop us a line below!
				</span>
			</div>

				<div className="custom-form-body">
					<div className="row">
						<div className="col-lg-7 col-md-7 col-sm-12 co-xs-12">
						<form className="contact100-form validate-form custom-form-body">
						<div className="wrap-input100 validate-input" data-validate="Name is required">
					<span className="label-input100">First Name:</span>
					<input className="input100" type="text" placeholder="Enter First Name" name="first_name" value={this.state.first_name} onChange={e => this.change(e)} required/>
					<span className="focus-input100"></span>
				</div>
				<div className="wrap-input100 validate-input" data-validate="Name is required">
					<span className="label-input100">Last Name:</span>
					<input className="input100" type="text" placeholder="Enter Last Name" name="last_name" value={this.state.last_name} onChange={e => this.change(e)} required/>
					<span className="focus-input100"></span>
				</div>

				<div className="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
					<span className="label-input100">Email:</span>
					<input className="input100" type="text" placeholder="Enter email addess" name="email" value={this.state.email} onChange={e => this.change(e)} required/>
					<span className="focus-input100"></span>
				</div>

				<div className="wrap-input100 validate-input" data-validate="Phone is required">
					<span className="label-input100">Phone:</span>
					<input className="input100" type="text" placeholder="Enter phone number" name="mobile" value={this.state.mobile} onChange={e => this.change(e)} required/>
					<span className="focus-input100"></span>
				</div>

				<div className="wrap-input100 validate-input" data-validate = "Message is required">
					<span className="label-input100">Message:</span>
					<textarea className="input100" placeholder="Your Comment..." name="msg" value={this.state.msg} onChange={e => this.change(e)} required></textarea>
					<span className="focus-input100"></span>
				</div>

				<div className="container-contact100-form-btn">
					<button className="contact100-form-btn" onClick = {e => this.onSubmit(e)}>
						<span>
							Submit
							<i className="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
						</span>
					</button>
				</div>
			</form>
						</div>

						<div className="col-lg-5 col-md-5 col-sm-12 col-xs-12">
						<div className="custom-right-content-contact">
                        <div className="row">
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h2>Contact Info</h2>
                            </div>
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <p> <i className="fa fa-map-marker" aria-hidden="true"></i>
                           <span className="custom-para-padding">  
							   
						   Edunomics Third Floor,Nikhil Heights,<br/>
                            Sajjan Wadi, Mulund East Mumbai
                           <br/>
                           Pincode 40081 
                            </span>
                            </p>
                            </div>
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <p> <i className="fa fa-phone" aria-hidden="true"></i>
                           &nbsp;&nbsp;&nbsp; +91 9767460825</p>
                            </div>
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <p><i className="fa fa-envelope-o" aria-hidden="true"></i>
                           &nbsp;&nbsp;&nbsp; hr@edunomics.in</p>
                            </div>
                        </div>
                    </div>
						</div>
					</div>
				</div>
			
		</div>
	</div>
        {/*end content here */}
      </div>        
      </React.Fragment>
    )
  }
}

export default Contact
