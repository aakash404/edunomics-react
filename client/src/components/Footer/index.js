import React, { Component } from 'react'
import './Footer.css'

export class Footer extends Component {
  render() {
    return (
      <div>
        <div className="custom-footer">
        <footer className="container">
            <p className="float-right"><a href="#"><i class="fa fa-arrow-circle-up fa-2x" aria-hidden="true"></i></a></p>
            <p className="text-center">&copy; 2017-2018 <a href="#">Edunomics</a>. All rights reserved.</p>
      </footer>
      </div>
      </div>
    )
  }
}

export default Footer
