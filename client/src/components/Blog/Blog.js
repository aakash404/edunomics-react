import React, { Component } from 'react';
import './Blog.css';
import {Link} from 'react-router-dom';
class Blog extends Component {
    render() {
        return (
            <div>
               <div className = "blog-content text-center">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h1>Clean Blog</h1>
                            </div>    
                        </div>

                    </div>
               </div>
               {/*blog content para */}
                  <div className="custom-blog-content">
                    <div className="container">
                        <div className="row">
                        <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <div className="blog-content-below-para">
                                <div className="row">
                                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <h3>Vision</h3>
                                    </div>
                                <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div className="blog-images-valid">
                                <img src = {require('../images/blog3.png')} className="img-fluid" />
                                 </div>   
                                </div> 
                                <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <p>Plastics are one of derivative of petrochemicals, unlike other derivatives of petrochemical it is generally thought of with a different perspective a positive and a negative.
                                        Plastic industry is reaching new elevations nowadays . He have almost everything manufactured in plastic let it be a small toothbrush to even parts of our vehicles everything in every zonal.
                                    </p>
                                </div>   
                                </div>
                                <hr />
                                 <div className="row">
                                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <h3>5 FAMOUS INFRASTRUCTURAL PROJECTS CRAWLING IN INDIA</h3>
                                    </div>
                                <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                               <p>
                               Indian architecture is as old as the history of civilization. Development of a country depends upon how much  urbanised it is .
											There is no point of explaining that how much importance the construction of the eye catching building and structures hold in the economic strength of the country.
											It not only adds a little flare to the beauty of a place but also gives employment to those thousands of unemployed youth who were starving for a single meal. ....											     
                                   </p>   
                                </div> 
                                <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                               <div className="blog-images-valid">
                                     <img src = {require('../images/blog4d.PNG')} className="img-fluid" />
                               </div>  
                                </div>   
                                </div>
                                <hr />
                                 <div className="row">
                                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <h3>The Burj Khalifa Project</h3>
                                    </div>

                                <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div className="blog-images-valid">
                                <img src = {require('../images/blog2.png')} className="img-fluid" />
                                 </div>    
                                </div> 
                                <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                  <p>The Burj Dubai Project is a multi-use development tower with a total floor area of 460,000 square meters that includes residential, hotel, commercial, office, entertainment, shopping, leisure, and parking facilities
                   				</p> 
                                </div>   
                                </div>
                                <hr />
                                <div className="row">
                                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <h3>No Delay</h3>
                                    </div>

                                <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                               <p>
                               The construction industry is one of the main sectors that provide important ingredient for the development of an economy. However, many projects experience extensive delays and thereby exceed initial time and cost estimates.
															  
                                   </p>   
                                </div> 
                                <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                   
        <div className="blog-images-valid">
                                <img src = {require('../images/blog6.png')} className="img-fluid" />
                                 </div>  
                                </div>   

                                </div>

                            </div>

                        </div>

                        <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div className="sidebar-custom-blog-content text-center">
                                <h5>Why India Need Bullet Train</h5>
                                <img src={require('../images/blog1a.PNG')} className="img-fluid" />
                                <p>Around and analyzing the major technological innovations where governments and private enterprises as
						well, are putting hundreds of billions of dollars, we could imply that a good number of such technological
						developments are related to the transportation sector
						</p>
                        <hr />
                        <h5>Smart City</h5>
                                <img src={require('../images/blog5.png')} className="img-fluid" />
                                <p>The term SMART CITY was coined towards the end of the 20th century. It is rooted in the implementation of user-friendly information and communication technologies developed by major industries for urban spaces. Its meaning has since been expanded to relate to the future of cities and their development.
					
						</p>
                        </div>

                        </div>
                        </div>
                    </div>
                  </div>  
               {/*end blog content para */}
            </div>
        );
    }
}

export default Blog;