import React, { Component } from 'react';
import './nav.css';
import {Link} from 'react-router-dom';
export class Navbar extends Component {
  render() {
    return (
      <div>
          <header>
            <nav className="navbar navbar-expand-md navbar-dark fixed-top navbar-default custom-edu-navbar">
               <Link to = "/"> <a className="navbar-brand" href="#">
                <img src= {require('../images/log-white.png')} className="img-fluid" width="250" />
                </a></Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse justify-content-end" id="navbarCollapse">
                <ul className="navbar-nav mr-auto custom-upper-navbar">
                    <li className="nav-item active">
                    
                    <Link to="/"><a className="nav-link">Home <span className="sr-only">(current)</span></a></Link>
                    </li>
                    <li className="nav-item">
                    <Link to="/About">
                    <a className="nav-link" href={"/About"}>About</a>
                    </Link>
                    </li>
                     <li className="nav-item">
                     <Link to="/Services">
                     <a className="nav-link" href={"/Services"}>Services</a>
                     </Link>
                    </li>
                     {/* <li className="nav-item">
                    <a className="nav-link" href="/#client">Clients</a>
                    </li> */}
                     <li className="nav-item">
                    <Link to="/Blog"> <a className="nav-link">Blog</a></Link>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link" href="#">Case Studies</a>
                    </li>
                    <li className="nav-item">
                    <Link to="/Career"> <a className="nav-link">Career</a></Link>
                    </li>
               <li className="nav-item">
               <Link to = "/Contact"><a className="nav-link" href={"/Contact"}>Contact</a></Link>
                    </li>
                </ul>
                </div>
            </nav>
        </header>
        
        
      </div>
    )
  }
}

export default Navbar
