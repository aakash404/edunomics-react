import React, { Component } from 'react';
import './App.css';
import {Route} from  'react-router-dom'
import Main from './components/Main';
import Navbar from './components/Navbar'
import Footer from './components/Footer';
import Blog from './components/Blog/Blog';
import Career from './components/Career/Career';
import { About } from './components/About/About';
import { Services } from './components/Services/Services';
import { Contact } from './components/Contact/Contact';
class App extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <Route path = "/" exact = {true} component = {Main} />
        <Route path = "/About" exact = {true} component = {About} />
        <Route path = "/Services" exact = {true} component = {Services} />
        <Route path = "/Blog" exact = {true} component = {Blog} />
        <Route path = "/Career" exact = {true} component = {Career} />
        <Route path = "/Contact" exact = {true} component  = {Contact} />
        <Footer />
      </div>
    );
  }
}

export default App;
